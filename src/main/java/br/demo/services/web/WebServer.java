package br.demo.services.web;

import br.demo.services.web.controller.HomeController;
import br.demo.services.web.controller.WebPessoaController;
import br.demo.services.web.controller.WebTipoController;
import br.demo.services.web.service.WebPessoaService;
import br.demo.services.web.service.WebTipoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * Created by chimbida on 2/23/2017.
 */

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(useDefaultFilters = false) // desabilita a bosta do component scanner
public class WebServer {

    public static final String PESSOA_SERVICE_URL = "http://PESSOA-SERVICE";

    public static final String TIPO_SERVICE_URL = "http://TIPO-SERVICE";


    public static void main(String[] args) {
        // Diz para procurar em web-server.properties or web-server.yml
        System.setProperty("spring.config.name", "web-server");
        SpringApplication.run(WebServer.class, args);
    }

    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public WebPessoaService pessoaService() {
        return new WebPessoaService(PESSOA_SERVICE_URL);
    }

    @Bean
    public WebTipoService tipoService() {
        return new WebTipoService(TIPO_SERVICE_URL);
    }

    @Bean
    public WebPessoaController pessoaController() {
        return new WebPessoaController(pessoaService());
    }

    @Bean
    public WebTipoController tipoController() {
        return new WebTipoController(tipoService());
    }

    @Bean
    public HomeController homeController() {
        return new HomeController();
    }


}
