package br.demo.services.web.model;

import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by chimbida on 2/27/2017.
 */

@JsonRootName("Tipo")
public class Tipo {

    protected Long id;

    protected String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "Tipo{" +
                "id=" + id +
                ", descricao='" + descricao + '\'' +
                '}';
    }
}
