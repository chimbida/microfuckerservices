package br.demo.services.web.model;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by chimbida on 22/02/2017.
 */

@JsonRootName("Pessoa")
public class Pessoa {

    protected Long id;

    protected String nome;

    protected String sobrenome;

    protected Tipo tipo;

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", sobrenome='" + sobrenome + '\'' +
                ", tipo=" + tipo +
                '}';
    }
}
