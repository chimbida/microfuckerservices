package br.demo.services.web.service;

import br.demo.services.web.model.Tipo;
import br.demo.services.web.model.Tipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/27/2017.
 */

@Service
public class WebTipoService {

    @Autowired
    @LoadBalanced
    protected RestTemplate restTemplate;

    protected String serviceUrl;

    protected Logger logger = Logger.getLogger(WebTipoService.class.getName());

    public WebTipoService(String serviceUrl) {
        this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
    }

    @PostConstruct
    public void demoOnly() {
        logger.warning("A fábrica do pedido RestTemplate é "
                + restTemplate.getRequestFactory().getClass());
    }

    public Tipo findById(Integer tipoId) {

        logger.info("findById() chamou: para " + tipoId);

        return restTemplate.getForObject(serviceUrl + "/tipo/{tipoId}",
                Tipo.class, tipoId);
    }

    public Tipo novo(Tipo tipo) {
        logger.info("novaTipo() chamada: Nome" + tipo.getDescricao());
        return restTemplate.postForObject(serviceUrl + "/tipo", tipo, Tipo.class);
    }

    public List<Tipo> byDescricaoContains(String descricao) {
        logger.info("byDescricaoContains() chamou:  para " + descricao);
        Tipo[] tipos = null;

        try {
            tipos = restTemplate.getForObject(serviceUrl
                    + "/tipo/descricao/{descricao}", Tipo[].class, descricao);
        } catch (HttpClientErrorException e) {
            // 404 Nothing found
        }

        if (tipos == null || tipos.length == 0)
            return null;
        else
            return Arrays.asList(tipos);
    }


    public List<Tipo> listaTipo() {
        logger.info("listaTipo() chamado");
        Tipo[] tipos = null;

        try {
            tipos = restTemplate.getForObject(serviceUrl
                    + "/tipo/list", Tipo[].class);
        } catch (HttpClientErrorException e) {
            // 404 Nothing found
        }

        if (tipos == null || tipos.length == 0)
            return null;
        else
            return Arrays.asList(tipos);

    }


}
