package br.demo.services.web.service;

import br.demo.services.web.model.Pessoa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/23/2017.
 */

@Service
public class WebPessoaService {

    @Autowired
    @LoadBalanced
    protected RestTemplate restTemplate;

    protected String serviceUrl;

    protected Logger logger = Logger.getLogger(WebPessoaService.class.getName());

    public WebPessoaService(String serviceUrl) {
        this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl : "http://" + serviceUrl;
    }

    @PostConstruct
    public void demoOnly() {
        logger.warning("A fábrica do pedido RestTemplate é "
                + restTemplate.getRequestFactory().getClass());
    }

    public Pessoa findById(Integer pessoaId) {

        logger.info("findById() chamou: para " + pessoaId);

        return restTemplate.getForObject(serviceUrl + "/pessoa/{pessoaId}",
                Pessoa.class, pessoaId);
    }

    public Pessoa nova(Pessoa pessoa) {
        logger.info("novaPessoa() chamada: Nome" + pessoa.getNome() + " Sobrenome " + pessoa.getSobrenome() + " Tipo" + pessoa.getTipo());
//        pessoa.setTipo();
        return restTemplate.postForObject(serviceUrl + "/pessoa", pessoa, Pessoa.class);
    }

    public List<Pessoa> byNomeContains(String nome) {
        logger.info("byNomeContains() chamou:  para " + nome);
        Pessoa[] pessoas = null;

        try {
            pessoas = restTemplate.getForObject(serviceUrl
                    + "/pessoa/nome/{nome}", Pessoa[].class, nome);
        } catch (HttpClientErrorException e) { 
            // 404 Nothing found
        }

        if (pessoas == null || pessoas.length == 0)
            return null;
        else
            return Arrays.asList(pessoas);
    }

    public List<Pessoa> bySobrenomeContains(String sobrenome) {
        logger.info("bySobrenomeContains() chamou:  para " + sobrenome);
        Pessoa[] pessoas = null;

        try {
            pessoas = restTemplate.getForObject(serviceUrl
                    + "/pessoa/sobrenome/{nome}", Pessoa[].class, sobrenome);
        } catch (HttpClientErrorException e) {
            // 404 Nothing found
        }

        if (pessoas == null || pessoas.length == 0)
            return null;
        else
            return Arrays.asList(pessoas);
    }

    public List<Pessoa> listaPessoa() {
        logger.info("listaPessoa() chamado");
        Pessoa[] pessoas = null;

        try {
            pessoas = restTemplate.getForObject(serviceUrl
                    + "/pessoa/list", Pessoa[].class);
        } catch (HttpClientErrorException e) {
            // 404 Nothing found
        }

        if (pessoas == null || pessoas.length == 0)
            return null;
        else
            return Arrays.asList(pessoas);

    }

}
