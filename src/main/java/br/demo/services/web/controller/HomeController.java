package br.demo.services.web.controller;

import br.demo.services.web.model.Pessoa;
import br.demo.services.web.model.Tipo;
import br.demo.services.web.service.WebPessoaService;
import br.demo.services.web.service.WebTipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/23/2017.
 */

@Controller()
@EnableAutoConfiguration
public class HomeController {


    // isso aqui deveria estar separado nas classes WebPessoaController e WebTipoController, mas tava usando para fazer testes com API


    // request tem q mudar, para conseguir pegar o erro

    protected Logger logger = Logger.getLogger(HomeController.class.getName());


    @Autowired
    protected WebPessoaService webPessoaService;

    @Autowired
    protected WebTipoService webTipoService;

    @RequestMapping("/")
    public ModelAndView home(Model model) {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("pessoa", webPessoaService.listaPessoa());
        mv.addObject("tipo", webTipoService.listaTipo());
        return mv;
    }

    @RequestMapping(value = "/pessoa", method= RequestMethod.GET)
    public ModelAndView pessoa(Model model) {
        ModelAndView mv = new ModelAndView("pessoa");
        mv.addObject("pessoa", webPessoaService.listaPessoa());
        return mv;
    }

    @RequestMapping(value = "/pessoa", method= RequestMethod.POST)
    public String salvarPessoa(@RequestBody Pessoa pessoa, BindingResult result, Model model) {


        logger.info("novaPessoa() chamado -> Nome: " + pessoa.getNome() + " Sobrenome: " + pessoa.getSobrenome() + " Tipo: " + pessoa.getTipo()
                + "\n RESULT: " + result.toString() + " \n"
                + "\n MODEL: " + model.toString() + " \n");

        if (result.hasErrors()) {
            logger.warning(result.toString());
            return "novapessoa";
        }
        webPessoaService.nova(pessoa);
        return "redirect:pessoa";
    }

    @RequestMapping(value = "/tipo", method= RequestMethod.GET)
    public ModelAndView tipo(Model model) {
        ModelAndView mv = new ModelAndView("tipo");
        mv.addObject("tipo", webTipoService.listaTipo());
        return mv;
    }


    @RequestMapping(value = "/tipo", method= RequestMethod.POST)
    public String salvarPessoa(Tipo tipo, BindingResult result, Model model) {
        if (result.hasErrors()) {
            System.out.print(result.toString());
            return "novotipo";
        }
        webTipoService.novo(tipo);
        return "redirect:tipo";
    }

    @RequestMapping("/novapessoa")
    public ModelAndView novaPessoa(Model model) {
        ModelAndView mv = new ModelAndView("novapessoa");
        mv.addObject("pessoa", new Pessoa());
        mv.addObject("tipo", webTipoService.listaTipo());
        return mv;
    }

    @RequestMapping("/novotipo")
    public ModelAndView novoTipo(Model model) {
        ModelAndView mv = new ModelAndView("novotipo");
        mv.addObject("tipo", new Tipo());
        return mv;
    }

}
