package br.demo.services.web.controller;

import br.demo.services.web.service.WebPessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/23/2017.
 */

@Controller()
public class WebPessoaController {

    @Autowired
    protected WebPessoaService webPessoaService;

    protected Logger logger = Logger.getLogger(WebPessoaController.class.getName());

    public WebPessoaController(WebPessoaService webPessoaService) {
        this.webPessoaService = webPessoaService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields("pessoaId", "textoProcura");
    }

//    @RequestMapping("/configuration")
//    public String goHome() {
//        return "index";
//    }
//
//
//    @RequestMapping("/configuration/{pessoaId}")
//    private String borId(Model model, @PathVariable("pessoaId") Integer pessoaId) {
//
//        logger.info("web-service porId() chamado: " + pessoaId);
//
//        Pessoa configuration = webPessoaService.findById(pessoaId);
//        logger.info("web-service porId() achou: " + configuration);
//        model.addAttribute("configuration", configuration);
//        return "configuration";
//    }
//
//    @RequestMapping("/configuration/list")
//    public String listaPessoa(Model model, lista) {
//        logger.info("web-service listaPessoa() chamado: ");
//
//        List<Pessoa> accounts = webPessoaService.
//        model.addAttribute("lista", lista);
//        return "accounts";
//    }
//
//    @RequestMapping("/configuration/nome/{text}")
//    public String nomeSearch(Model model, @PathVariable("text") String name) {
//        logger.info("web-service byNome() invoked: " + name);
//
//        List<Account> accounts = accountsService.byOwnerContains(name);
//        logger.info("web-service byOwner() found: " + accounts);
//        model.addAttribute("search", name);
//        if (accounts != null)
//            model.addAttribute("accounts", accounts);
//        return "accounts";
//    }

    @RequestMapping("/api/pessoa")
    public @ResponseBody
    List<br.demo.services.web.model.Pessoa> listaPessoas() {

        List<br.demo.services.web.model.Pessoa> lista = webPessoaService.listaPessoa();
        logger.info("web-service listaPessoas() chamado");

        return lista;
    }



}
