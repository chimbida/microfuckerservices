package br.demo.services.web.controller;

import br.demo.services.web.model.Tipo;
import br.demo.services.web.service.WebTipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/27/2017.
 */

@Controller()
public class WebTipoController {

    @Autowired
    protected WebTipoService webTipoService;

    protected Logger logger = Logger.getLogger(WebTipoController.class.getName());

    public WebTipoController(WebTipoService webTipoService) {
        this.webTipoService = webTipoService;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields("tipoId", "textoProcura");
    }

//    @RequestMapping("/configuration")
//    public String goHome() {
//        return "index";
//    }
//
//
//    @RequestMapping("/configuration/{tipoId}")
//    private String borId(Model model, @PathVariable("tipoId") Integer tipoId) {
//
//        logger.info("web-service porId() chamado: " + tipoId);
//
//        Tipo configuration = webTipoService.findById(tipoId);
//        logger.info("web-service porId() achou: " + configuration);
//        model.addAttribute("configuration", configuration);
//        return "configuration";
//    }
//
//    @RequestMapping("/configuration/list")
//    public String listaTipo(Model model, lista) {
//        logger.info("web-service listaTipo() chamado: ");
//
//        List<Tipo> accounts = webTipoService.
//        model.addAttribute("lista", lista);
//        return "accounts";
//    }
//
//    @RequestMapping("/configuration/nome/{text}")
//    public String nomeSearch(Model model, @PathVariable("text") String name) {
//        logger.info("web-service byNome() invoked: " + name);
//
//        List<Account> accounts = accountsService.byOwnerContains(name);
//        logger.info("web-service byOwner() found: " + accounts);
//        model.addAttribute("search", name);
//        if (accounts != null)
//            model.addAttribute("accounts", accounts);
//        return "accounts";
//    }

    @RequestMapping("/api/tipo")
    public @ResponseBody
    List<Tipo> listaTipos() {

        List<br.demo.services.web.model.Tipo> lista = webTipoService.listaTipo();
        logger.info("web-service listaTipos() chamado");

        return lista;
    }
    
    
}
