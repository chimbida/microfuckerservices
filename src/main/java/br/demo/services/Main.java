package br.demo.services;

import br.demo.services.pessoa.PessoaServer;
import br.demo.services.registration.RegistrationServer;
import br.demo.services.tipo.TipoServer;
import br.demo.services.web.WebServer;

/**
 * Created by chimbida on 2/23/2017.
 */

// Esse cara que permite que cada servidor seja chamado separadamente

public class Main {

    public static void main(String[] args) {

        String nomeServidor = "VAZIO";

        switch (args.length) {
            case 2:

                //altera a porta definida no arquivo de configuração padrão
                System.setProperty("server.port", args[1]);

            case 1:
                nomeServidor = args[0].toLowerCase();
                break;

            default:
                usando();
                return;
        }

        if (nomeServidor.equals("registration") || nomeServidor.equals("reg")) {
            RegistrationServer.main(args);
        } else if (nomeServidor.equals("pessoa")) {
            PessoaServer.main(args);
        } else if (nomeServidor.equals("web")) {
            WebServer.main(args);
        } else if (nomeServidor.equals("tipo")) {
            TipoServer.main(args);
        } else {
            System.out.println("Tipo de servidor desconhecido: " + nomeServidor);
            usando();
        }

    }

    protected static void usando() {
        System.out.println("Como usars: java -jar ... <server-name> [server-port]");
        System.out.println(
                "     onde server-name é 'reg', 'registration', " + "'pessoa', 'tipo' ou 'web' e server-port > 1024");
    }

}
