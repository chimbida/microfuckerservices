package br.demo.services.pessoa;

import br.demo.aplication.MainConfiguration;
import br.demo.aplication.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import java.util.logging.Logger;

/**
 * Created by chimbida on 2/23/2017.
 */

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(MainConfiguration.class)
public class PessoaServer {

    @Autowired
    protected PessoaRepository pessoaRepository;

    protected Logger logger = Logger.getLogger(PessoaServer.class.getName());


    public static void main(String[] args) {
        System.setProperty("spring.config.name", "pessoa-server");
        SpringApplication.run(PessoaServer.class,args);
    }


}
