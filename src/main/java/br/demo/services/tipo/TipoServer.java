package br.demo.services.tipo;

import br.demo.aplication.MainConfiguration;
import br.demo.aplication.repository.TipoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

import java.util.logging.Logger;

/**
 * Created by chimbida on 2/27/2017.
 */

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(MainConfiguration.class)
public class TipoServer {

    @Autowired
    protected TipoRepository tipoRepository;

    protected Logger logger = Logger.getLogger(TipoServer.class.getName());


    public static void main(String[] args) {
        System.setProperty("spring.config.name", "tipo-server");
        SpringApplication.run(TipoServer.class,args);
    }


}
