package br.demo.aplication.repository;

import br.demo.aplication.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chimbida on 22/02/2017.
 */

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    // pega configuration pelo ID
    public Pessoa findById(int id);

    // lista pessoas contento algum nome
    public List<Pessoa> findByNomeContainingIgnoreCase(String nome);

    public List<Pessoa> findBySobrenomeContainingIgnoreCase(String nome);

    // conta quantidade de pessoas
    @Query("SELECT count(*) from Pessoa")
    public int contaPessoas();

}
