package br.demo.aplication.repository;

import br.demo.aplication.model.Tipo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chimbida on 2/27/2017.
 */

@Repository
public interface TipoRepository extends JpaRepository<Tipo, Long> {

    // pega tipo pelo ID
    public Tipo findById(int id);

    // lista tipos contento algum nome
    public List<Tipo> findByDescricaoContainingIgnoreCase(String nome);

    // conta quantidade de tipos
    @Query("SELECT count(*) from Tipo")
    public int contaTipos();
    
}
