package br.demo.aplication.controller;

import br.demo.aplication.excecao.PessoaNotFoundException;
import br.demo.aplication.model.Pessoa;
import br.demo.aplication.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/23/2017.
 */

@RestController
public class PessoaController {

    protected Logger logger = Logger.getLogger(PessoaController.class.getName());


    @Autowired
    protected PessoaRepository pessoaRepository;

//    @Autowired
//    public PessoaController(PessoaRepository pessoaRepository) {
//        this.pessoaRepository.contaPessoas();
//        logger.info("O Repositorio de Pessoas diz que existem " + pessoaRepository.contaPessoas() + " pessoas no sistema.");
//    }

    @RequestMapping("/pessoa/{pessoaId}")
    public Pessoa porId(@PathVariable("pessoaId") Integer pessoaId) {
        logger.info("servico-configuration porId() chamou: " + pessoaId);
        Pessoa pessoa = pessoaRepository.findById(pessoaId);
        logger.info("servico-configuration porId() achou: " + pessoa);

        if (pessoa == null)
            throw new PessoaNotFoundException(pessoaId.toString());
        else {
            return pessoa;
        }
    }

    @RequestMapping("/pessoa/nome/{nome}")
    public List<Pessoa> byNome(@PathVariable("nome") String nome) {
        logger.info("pessoas-service byNome() chamado: "
                + pessoaRepository.getClass().getName() + " for "
                + nome);

        List<Pessoa> pessoas = pessoaRepository.findByNomeContainingIgnoreCase(nome);
        logger.info("pessoas-service byOwner() found: " + pessoas);

        if (pessoas == null || pessoas.size() == 0)
            throw new PessoaNotFoundException(nome);
        else {
            return pessoas;
        }
    }

    @RequestMapping("/pessoa/sobrenome/{nome}")
    public List<Pessoa> bySorenome(@PathVariable("nome") String nome) {
        logger.info("pessoas-service bySorenome() chamado: "
                + pessoaRepository.getClass().getName() + " for "
                + nome);

        List<Pessoa> pessoas = pessoaRepository.findBySobrenomeContainingIgnoreCase(nome);
        logger.info("pessoas-service byOwner() found: " + pessoas);

        if (pessoas == null || pessoas.size() == 0)
            throw new PessoaNotFoundException(nome);
        else {
            return pessoas;
        }
    }

    @RequestMapping("/pessoa/list")
    public List<Pessoa> listaPessoa() {

        logger.info("servico-configuration listaPessoa() chamado");

        List<Pessoa> pessoa = pessoaRepository.findAll();

        return pessoa;
    }

    @RequestMapping(value = "/pessoa", method= RequestMethod.POST)
    public ResponseEntity<Pessoa> novaPessoa(@RequestBody Pessoa pessoa, BindingResult result, Model model) {

        logger.info("servico-configuration novaPessoa() chamado");

        Pessoa p = new Pessoa();
        p.setNome(pessoa.getNome());
        p.setSobrenome(pessoa.getSobrenome());
        p.setTipo(pessoa.getTipo());

        pessoaRepository.save(p);

        return new ResponseEntity<Pessoa>(p, HttpStatus.OK);
    }
}