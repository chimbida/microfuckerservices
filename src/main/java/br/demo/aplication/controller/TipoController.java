package br.demo.aplication.controller;

import br.demo.aplication.excecao.TipoNotFoundException;
import br.demo.aplication.model.Tipo;
import br.demo.aplication.repository.TipoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chimbida on 2/27/2017.
 */

@RestController
public class TipoController {

    protected Logger logger = Logger.getLogger(TipoController.class.getName());


    @Autowired
    protected TipoRepository tipoRepository;

//    @Autowired
//    public TipoController(TipoRepository tipoRepository) {
//        this.tipoRepository.contaTipos();
//        logger.info("O Repositorio de Tipos diz que existem " + tipoRepository.contaTipos() + " tipos no sistema.");
//    }

    @RequestMapping("/tipo/{tipoId}")
    public Tipo porId(@PathVariable("tipoId") Integer tipoId) {
        logger.info("servico-tipo porId() chamou: " + tipoId);
        Tipo tipo = tipoRepository.findById(tipoId);
        logger.info("servico-tipo porId() achou: " + tipo);

        if (tipo == null)
            throw new TipoNotFoundException(tipoId.toString());
        else {
            return tipo;
        }
    }

    @RequestMapping("/tipo/descricao/{descricao}")
    public List<Tipo> byDescricao(@PathVariable("descricao") String descricao) {
        logger.info("tipos-service byDescricao() chamado: "
                + tipoRepository.getClass().getName() + " for "
                + descricao);

        List<Tipo> tipos = tipoRepository.findByDescricaoContainingIgnoreCase(descricao);
        logger.info("tipos-service byDescricao() found: " + tipos);

        if (tipos == null || tipos.size() == 0)
            throw new TipoNotFoundException(descricao);
        else {
            return tipos;
        }
    }

    @RequestMapping("/tipo/list")
    public List<Tipo> listaTipo() {

        logger.info("servico-tipo listaTipo() chamado");

        List<Tipo> tipo = tipoRepository.findAll();

        return tipo;
    }

    @RequestMapping(value = "/tipo", method= RequestMethod.POST)
    public ResponseEntity<Tipo> novaTipo(@RequestBody Tipo tipo, BindingResult result, Model model) {

        logger.info("servico-configuration novaTipo() chamado");

        Tipo t = new Tipo();
        t.setDescricao(tipo.getDescricao());

        tipoRepository.save(t);

        return new ResponseEntity<Tipo>(t, HttpStatus.OK);
    }
    
}
