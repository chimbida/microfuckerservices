package br.demo.aplication.excecao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by chimbida on 2/27/2017.
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TipoNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2466924547524344053L;

    public TipoNotFoundException(String tipo) {
        super("Tipo com ID: " + tipo + " não existe!");
    }

}
