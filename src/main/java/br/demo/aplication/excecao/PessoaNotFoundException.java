package br.demo.aplication.excecao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by chimbida on 2/23/2017.
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PessoaNotFoundException extends RuntimeException {


    private static final long serialVersionUID = -4034717897629892725L;

    public PessoaNotFoundException(String pessoa) {
        super("Pessoa com ID: " + pessoa + " não existe!");
    }

}
