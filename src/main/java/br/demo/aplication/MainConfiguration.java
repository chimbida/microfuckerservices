package br.demo.aplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Created by chimbida on 2/24/2017.
 */

@Configuration
@ComponentScan
@EntityScan("br.demo.aplication.model")
@EnableJpaRepositories("br.demo.aplication.repository")
@PropertySource("classpath:db-config.properties")
public class MainConfiguration {

    @Bean
    public DriverManagerDataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost/microfuckerservices?useSSL=false");
        dataSource.setUsername( "root" );
        dataSource.setPassword( "root" );
        return dataSource;
    }


}
